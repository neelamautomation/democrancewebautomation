
package democrance.webautomation.pageObjects;

import java.time.Duration;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import democrance.webautomation.base.BaseClass;

public class LoginPage extends BaseClass{

	 public LoginPage(){
	 wait= new WebDriverWait(driver,Duration.ofSeconds(10));
	 PageFactory.initElements(driver, this); 
	 }
	 	
@FindBy(xpath="//input[@name='username']")
 private WebElement Username;

@FindBy(xpath="//input[@name='password']")
private WebElement Password;

@FindBy(xpath="//button[@type='submit']")
private WebElement loginBtn;

@FindBy(xpath="//h5[text()='Login']")
private WebElement loginText;

public ProductsInformationPage logintoApplication(String username, String password) {
	wait.until(ExpectedConditions.visibilityOf(Username));
	Username.sendKeys(username);
	Password.sendKeys(password);
	loginBtn.click();
	return new ProductsInformationPage();
}

public String loginTextVerification() {
	String actualLoginText=loginText.getText();
	return actualLoginText;
}
}
