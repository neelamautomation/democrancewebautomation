package democrance.webautomation.base;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseClass {
	
	public static WebDriver driver ;
	public static Properties prop;
	public static WebDriverWait wait;
	public static Logger log;
	
	public BaseClass() {
		log=LoggerFactory.getLogger(BaseClass.class);
		   
		try {
		prop = new Properties();
		FileInputStream fis= new FileInputStream(System.getProperty("user.dir")+"//src//main//java//deriv//webautomation//config//config.properties");
		log.info("Properties file is loaded");
		prop.load(fis);
		}
		catch(FileNotFoundException e) {
			 e.printStackTrace();
		}
		catch(IOException e) {
			 e.printStackTrace();
		}
		
	}
	public void initialization() throws IOException {
		  log.info("Reading Brosername from properties file");
		  String browserName=prop.getProperty("BrowserName");
		  
		  if(browserName.equalsIgnoreCase("chrome")) {
				WebDriverManager.chromedriver().setup();
				driver= new ChromeDriver();
		  }
		  else if(browserName.equalsIgnoreCase("Firefox")) {
			     FirefoxOptions fo= new FirefoxOptions();
			     driver=new FirefoxDriver();
		  }
		  log.info("Maximizing the window");
		  driver.manage().window().maximize();
		  log.info("Deleting all cookies");
		  driver.manage().deleteAllCookies();
		  driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		  log.info("Launching the application");
		  driver.get(prop.getProperty("URL"));
	}


	
	public String getScreenshot(String testcaseName) throws IOException {
		TakesScreenshot screenShot= (TakesScreenshot)driver;
		File src=screenShot.getScreenshotAs(OutputType.FILE);
		File des= new File(System.getProperty("user.dir")+ "\\Screenshots\\"+ testcaseName+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return System.getProperty("user.dir")+ "\\Screenshots\\"+ testcaseName+".png";
	}
	

     
}

