package democrance.webautomation.pageObjects;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import democrance.webautomation.base.BaseClass;


public class MyInfoPage extends BaseClass{
	
	public MyInfoPage() {
		 PageFactory.initElements(driver, this);
		 wait= new WebDriverWait(driver,Duration.ofSeconds(10));
	}
	
	@FindBy(xpath="(//input[@class='oxd-input oxd-input--active'])[6]")
	private WebElement dateOfBirthField;
	
	@FindBy(xpath="(//i[contains(@class,'oxd-icon') and contains(@class,'oxd-date-input-icon')])[2]")
	private WebElement calendarIcon;
	
	@FindBy(xpath="//li[@class='oxd-calendar-selector-month']")
	WebElement monthDropDown;
	
	@FindBy(xpath="//li[@class='oxd-calendar-selector-year']")
	WebElement yearDropDown;
	
	 @FindBy(xpath="//div[@class='oxd-calendar-date']") 
	 List<WebElement>listOfDates;
	 
	@FindBy(xpath="//ul[@class='oxd-calendar-selector']/li/ul/li")
	List<WebElement> listOfMonths;
	
	@FindBy(xpath="//ul[@class='oxd-calendar-selector']/li[2]/ul/li")
	List<WebElement> listOfYears;
	
	@FindBy(xpath="(//button[normalize-space()='Save'])[1]")
	WebElement saveBtn;
	
	@FindBy(xpath="//label[text()='Date of Birth']")
	WebElement dateOfBirthLabel;
	
	public String dateOfBirthFieldIsFilledIn(){
		wait.until(ExpectedConditions.visibilityOf(dateOfBirthLabel));
		wait.until(ExpectedConditions.attributeToBeNotEmpty(dateOfBirthField, "value"));
		
	}
	
	public boolean isMyInfoPageLoaded() {
		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState === 'complete';"));
		boolean isPageLoaded = (boolean) ((JavascriptExecutor) driver).executeScript("return document.readyState === 'complete'");
		return  isPageLoaded;
	}
	
	
	public String clickAndSelectDateFromCalendar(){
		log.info("Click on calendar Icon");
		wait.until(ExpectedConditions.elementToBeClickable(calendarIcon)).click();
		log.info("Click on month dropdown of calendar Icon");
		wait.until(ExpectedConditions.elementToBeClickable(monthDropDown)).click();
		log.info("Retrieving list of months");
		int countOfMonths=listOfMonths.size();
		for(int i=0;i<countOfMonths;i++) {
			String month=listOfMonths.get(i).getText();
			if(month.equalsIgnoreCase("June")) {
				listOfMonths.get(i).click();
				break;
			}
		}
	   log.info("Clicking on year dropdown of calendar");
	   yearDropDown.click();
	   log.info("Retrieving list of years");
	   int countOfYears=listOfYears.size();
	   for(int i=0;i<countOfYears;i++) {
		   String year=listOfYears.get(i).getText();
		   if(year.equalsIgnoreCase("1991")) {
			   listOfYears.get(i).click();
			   break;
			}
	   }
	   log.info("Retrieving list of dates");  
	   int dates=listOfDates.size();
	    for(int i=0;i<dates;i++) {
			   String date=listOfDates.get(i).getText();
			   if(date.equalsIgnoreCase("6")) {
				   listOfDates.get(i).click();
				   break;
				}
		   }
	    
	    dateOfBirthField.sendKeys(Keys.TAB);
	    dateOfBirthField.sendKeys(Keys.TAB);
	    log.info("Clicking on save button");
	    wait.until(ExpectedConditions.elementToBeClickable(saveBtn));
	    saveBtn.click();
	    
	}	

