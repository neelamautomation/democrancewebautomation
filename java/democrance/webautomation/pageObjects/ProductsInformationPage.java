package democrance.webautomation.pageObjects;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import democrance.webautomation.base.BaseClass;


public class ProductsInformationPage extends BaseClass{

	
	public ProductsInformationPage() {
		   PageFactory.initElements(driver,this);
	}
	
	
	
	@FindBy(xpath="//input[@data-e2e='field-insured_age']")
    private WebElement dateOfBirthField;
	
	@FindBy(xpath="(//div[@class='control']//span/select)[1]")
	private WebElement monthDropDown;
	

	@FindBy(xpath="(//div[@class='control']//span/select)[2]")
	private WebElement yearDropDown;
	
	//@FindBy(xpath="//div/a[@class='datepicker-cell is-selectable']")
	//WebElement dayDropDown;
	
	public void selectDate(String month, String day, String year) {
	   
	    String expMonth = String.format("//div[@class='control']//select/option[normalize-space()='%s']", month);
	    WebElement monthOption = driver.findElement(By.xpath(expMonth));
	    
	    String expYear = String.format("//div[@class='control']//select/option[normalize-space()='%s']", year);
	    WebElement yearOption = driver.findElement(By.xpath(expYear));

	   
	    String expDay = String.format("//div[@class='datepicker-cell is-unselectable']/span[text()='%s']", day);
	    WebElement dayOption = driver.findElement(By.xpath(expDay));

	    monthDropDown.click();
	    monthOption.click();
	    
	    yearDropDown.click();
	    yearOption.click();

	    dayOption.click();
	}
	
	
	public String dateOfBirthIsFilled() {
		String actualDate=dateOfBirthField.getAttribute("value");
	    log.info("Value of updated date of birth is "+actualDate);
	    return actualDate;
	}
	
	
	public void clickOnDateOfBirthCalendar(){
		dateOfBirthField.click();
	}
	
	public String dateOfBirthIsEmpty() {
		String DateOfBirth=dateOfBirthField.getAttribute("value");
		log.info("Value of prefilled date is "+DateOfBirth);
		return DateOfBirth;
	}

	
	
}