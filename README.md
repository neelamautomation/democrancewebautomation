
## Project Title
Web Automation Project - Democrance

## Project Overview
This project focuses on web automation testing for end to end journey of insurance policy.

## Project Structure
The project aims to automate web interactions where hybrid framework is employed, incorporating Page Object Model (POM) and Object-Oriented Programming (OOPs) concepts.

- Page Objects (POM)
- Base Class
- Configuration Properties
- Helper Classes
- Test Cases and Test Components
- Reporting and Screenshots

## Tech Stack

Programming Language:Java
Automation Framework: Hybrid Framework
Automation Tool: Selenium WebDriver
Build Management Tool: Maven
Version Control Tool: Git
Testing Framework: TestNG
Logging: Log4j
Reporting: Extent Reports


## Dependencies & External Libraries

- Selenium: 4.6.0
- Webdriver Manager: 5.5.3
- TestNG: 7.7.1
- Extent Reports: 5.0.9
- SLF4J API: 1.6.1
- SLF4J Log4j Binding: 1.6.1
- Log4j: 1.2.16


## Page Classes
- PaymentPage.java
- PaymentReferencePage.java
- PolicyIssuanceAndPaymentPage.java
- ProductsInformationPage.java
- ProductsPage.java
- SummaryPage.java
- TermsConditionsPage.java

## Test Classes
- PaymentTest.java
- PaymentReferenceTest.java
- PolicyIssuanceAndPaymentTest.java
- ProductsInformationTest.java
- ProductsTest.java
- SummaryTest.java
- TermsConditionsTest.java


Note 01: This solution is uploaded on gitlab private repository.

Note 02: Pipeline is not yet fully configured. Idea is create a run the testng.xml using maven and headless browser. 
