package com.democrance.webautomation.pageObjects;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.democrance.webautomation.base.BaseClass;
import com.democrance.webautomation.helper.DateUtil;
import com.democrance.webautomation.helper.LocatorUtil;
import com.democrance.webautomation.helper.PropertyFileReader;

public class PolicyIssuanceAndPaymentPage extends BaseClass {

	PropertyFileReader propertyFileReader;

	public PolicyIssuanceAndPaymentPage() {
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, Duration.ofSeconds(10));
	}

	@FindBy(xpath = "//h1[text()='Policy issuance and Payment']")
	private WebElement policyIssuanceAndPaymentText;

	@FindBy(xpath = "//span[text()='Mrs']")
	private WebElement title;

	@FindBy(xpath = "//input[@name='full_name']")
	private WebElement fullNameTxtBox;

	@FindBy(xpath = "//input[@data-e2e='field-nationality']")
	private WebElement nationalityDropDown;

	@FindBy(xpath = "//a[@class='dropdown-item']/div/span[text()='India']")
	private WebElement selectNationality;

	@FindBy(xpath = "//input[@name='email_address']")
	private WebElement emailAddressTxtBox;

	@FindBy(xpath = "//input[@placeholder='Mobile Number']")
	private WebElement mobileNumberTxtBox;

	@FindBy(xpath = "//input[@data-e2e='field-emirates_id']")
	private WebElement emiratesIdTxtBox;

	@FindBy(xpath = "(//div[@class='control']//span/select)[1]")
	private WebElement monthDropDown;

	@FindBy(xpath = "(//div[@class='control']//span/select)[2]")
	private WebElement yearDropDown;

	@FindBy(xpath = "//input[@data-e2e='field-emirates_id_expiry_date']")
	private WebElement emiratesIdExpiryDateField;

	@FindBy(xpath = "//div[@class='button touch-buttons is-small is-active']")
	private WebElement titleIsSelected;

	@FindBy(xpath = "//button[text()='Next: Summary']")
	private WebElement summaryNextBtn;

	@FindBy(xpath = "//button[text()='Back to Form']")
	private WebElement backToFormBtn;

	public String enterTitle() {
		title.click();
		return title.getText();
	}

	public String enterFullName() throws IOException {
		propertyFileReader = new PropertyFileReader();
		String fullName = propertyFileReader.readFullName();
		fullNameTxtBox.sendKeys(fullName);
		String expectedName = fullNameTxtBox.getAttribute("value");
		return expectedName;
	}

	public String enterNationality() {
		nationalityDropDown.click();
		selectNationality.click();
		String expectedNationality = nationalityDropDown.getAttribute("value");
		return expectedNationality;
	}

	public String enterEmailAddress() throws IOException {
		propertyFileReader = new PropertyFileReader();
		String email = propertyFileReader.readEmail();
		emailAddressTxtBox.sendKeys(email);
		String expectedEmailAddress = emailAddressTxtBox.getAttribute("value");
		return expectedEmailAddress;
	}

	public String enterMobileNumber() throws IOException {
		propertyFileReader = new PropertyFileReader();
		String phoneNumber = propertyFileReader.readPhoneNumber();
		mobileNumberTxtBox.sendKeys(phoneNumber);
		String actualMobileNumber = mobileNumberTxtBox.getAttribute("value");
		String phoneNumberWithoutSpaces = actualMobileNumber.replaceAll("\\s", "");
		return phoneNumberWithoutSpaces;
	}

	public String appendCountryCodetoMobileNumber() throws IOException {
		String mobileNumber = enterMobileNumber();
		String appendCountryCodeMobileNumber = "+971" + mobileNumber.substring(1);
		return appendCountryCodeMobileNumber;
	}

	public String enterEmirateID() throws IOException {
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("arguments[0].scrollIntoView(true);", emiratesIdTxtBox);
		emiratesIdTxtBox.click();
		propertyFileReader = new PropertyFileReader();
		String emiratesId = propertyFileReader.readEmiratesId();
		emiratesIdTxtBox.sendKeys(emiratesId);
		String actualEmiratesId = emiratesIdTxtBox.getAttribute("value");
		return actualEmiratesId;
	}

	public void selectDate(String month, String day, String year) {
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("arguments[0].scrollIntoView(true);", emiratesIdExpiryDateField);
		emiratesIdExpiryDateField.click();
		WebElement monthOption = LocatorUtil.createDynamicXPath(driver, "//select/option[normalize-space()='%s']",
				month);
		monthDropDown.click();
		monthOption.click();
		WebElement yearOption = LocatorUtil.createDynamicXPath(driver, "//select/option[normalize-space()='%s']", year);
		yearDropDown.click();
		yearOption.click();
		WebElement dayOption = LocatorUtil.createDynamicXPath(driver,
				"//a[@class='datepicker-cell is-selectable']/span[text()='%s']", day);
		dayOption.click();
	}

	public String getEmiratesIDExpiryDate() {
		wait.until(ExpectedConditions.visibilityOf(emiratesIdExpiryDateField));
		String actualDate = emiratesIdExpiryDateField.getAttribute("value");
		log.info("Value of updated emirates Id expiry Date is " + actualDate);
		return actualDate;
	}

	public String getEmiratesIdExpiryDateInYYYYMMDD() {
		String emiratesExpiryDate = getEmiratesIDExpiryDate();
		return DateUtil.convertDateFromDDMMYYYYToYYYYMMDD(emiratesExpiryDate);
	}

	public boolean isTitleSelected() {
		return titleIsSelected.isEnabled();
	}

	public String policyIssuanceAndPaymentTextVerification() {
		wait.until(ExpectedConditions.visibilityOf(policyIssuanceAndPaymentText));
		String actualText = policyIssuanceAndPaymentText.getText();
		return actualText;
	}

	public SummaryPage clickOnNextSummaryBtn() {
		summaryNextBtn.click();
		return new SummaryPage();
	}

	public TermsConditionsPage clickBackToFormBtn() {
		((JavascriptExecutor) driver).executeAsyncScript("setTimeout(arguments[arguments.length - 1], 3000);");
		backToFormBtn.click();
		return new TermsConditionsPage();
	}

}
