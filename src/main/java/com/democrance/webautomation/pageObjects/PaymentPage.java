package com.democrance.webautomation.pageObjects;

import java.time.Duration;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.democrance.webautomation.base.BaseClass;

public class PaymentPage extends BaseClass {

	public PaymentPage() {
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, Duration.ofSeconds(10));
	}

	@FindBy(xpath = "//span[text()='Generate Invoice']")
	private WebElement generateInvoiceOption;

	@FindBy(xpath = "//div/h2[text()='Select a Payment Option']")
	private WebElement paymentTextVerification;

	public PaymentReferencePage clickOnGenerateInvoiceOption() {
		generateInvoiceOption.click();
		return new PaymentReferencePage();
	}

	public String paymentTextVerification() {
		wait.until(ExpectedConditions.visibilityOf(paymentTextVerification));
		return paymentTextVerification.getText();

	}
}
