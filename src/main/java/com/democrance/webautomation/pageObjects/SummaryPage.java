package com.democrance.webautomation.pageObjects;

import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.democrance.webautomation.base.BaseClass;

public class SummaryPage extends BaseClass {

	ProductsInformationPage productsInformationPage;

	public SummaryPage() {
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, Duration.ofSeconds(10));
	}

	@FindBy(xpath = "//h2[contains(normalize-space(), 'Quotation Reference Number')]")
	private WebElement summaryPageTextVerification;

	@FindBy(xpath = "//h1[text()='Information']")
	private WebElement informationTabText;

	@FindBy(xpath = "(//div[@class='flexcell']/span)[1]")
	private WebElement summaryPageDateOfBirth;

	@FindBy(xpath = "(//div[@class='flexcell']/span)[2]")
	private WebElement policyEffectiveDate;

	@FindBy(xpath = "(//div[@class='flexcell']/span)[3]")
	private WebElement salaryRange;

	@FindBy(xpath = "(//div[@class='flexcell']/span)[4]")
	private WebElement premiumPaymentOption;

	@FindBy(xpath = "(//div[@class='flexcell']/span)[5]")
	private WebElement title;

	@FindBy(xpath = "(//div[@class='flexcell']/span)[6]")
	private WebElement fullName;

	@FindBy(xpath = "(//div[@class='flexcell']/span)[7]")
	private WebElement nationality;

	@FindBy(xpath = "(//div[@class='flexcell']/span)[8]")
	private WebElement emailAddress;

	@FindBy(xpath = "(//div[@class='flexcell']/span)[9]")
	private WebElement mobileNumber;

	@FindBy(xpath = "(//div[@class='flexcell']/span)[10]")
	private WebElement emiratesIdNumber;

	@FindBy(xpath = "(//div[@class='flexcell']/span)[11]")
	private WebElement emiratesIdExpiryDate;

	@FindBy(xpath = "//button[text()='Confirm and Buy Now']")
	private WebElement confirmAndBuyBtn;

	public String informationTextVerification() {
		return informationTabText.getText();
	}

	public String getDateOfBirth() {
		return summaryPageDateOfBirth.getText();
	}

	public String getPolicyEffectiveDate() {
		return policyEffectiveDate.getText();
	}

	public String getSalaryRange() {
		return salaryRange.getText();
	}

	public String getPremiumPaymentOption() {
		return premiumPaymentOption.getText();
	}

	public String getTitle() {
		return title.getText();
	}

	public String getFullName() {
		return fullName.getText();
	}

	public String getNationality() {
		return nationality.getText();
	}

	public String getEmailAddress() {
		return emailAddress.getText();
	}

	public String getMobileNumber() {
		return mobileNumber.getText();
	}

	public String getEmiratesIdNumber() {
		return emiratesIdNumber.getText();
	}

	public String getEmiratesIdExpiryDate() {
		return emiratesIdExpiryDate.getText();
	}

	public PaymentPage clickOnConfirmAndBuyBtn() {
		((JavascriptExecutor) driver).executeAsyncScript("setTimeout(arguments[arguments.length - 1], 6000);");
		confirmAndBuyBtn.click();
		return new PaymentPage();
	}

}
