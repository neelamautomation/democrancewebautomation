package com.democrance.webautomation.pageObjects;

import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.democrance.webautomation.base.BaseClass;

public class TermsConditionsPage extends BaseClass {

	public TermsConditionsPage() {
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, Duration.ofSeconds(10));
	}

	@FindBy(xpath = "//div/h1[text()='Terms & Conditions']")
	private WebElement termsConditionsText;

	@FindBy(xpath = "(//span[@class='control-label']/span)[1]")
	private WebElement termsConditionCheckBox1;

	@FindBy(xpath = "(//span[@class='control-label']/span)[2]")
	private WebElement termsConditionCheckBox2;

	@FindBy(xpath = "//button[text()='Next: Policy issuance and Payment']")
	private WebElement NextPolicyIssuanceAndPaymentBtn;

	@FindBy(xpath = "//button[text()='Back to Form']")
	private WebElement backToFormBtn;

	@FindBy(xpath = "//button[@class='button button-next is-primary is-expanded unavailable']")
	private WebElement PolicyIssuanceAndPaymentBtnIsDisabled;

	@FindBy(xpath = "(//input[@true-value='true'])[1]")
	private WebElement termsConditionCheckBox1IsSelected;

	@FindBy(xpath = "(//input[@true-value='true'])[2]")
	private WebElement termsConditionCheckBox2IsSelected;

	public String TermsAndConditionsTextVerification() {
		wait.until(ExpectedConditions.visibilityOf(termsConditionsText));
		String expectedText = termsConditionsText.getText();
		return expectedText;
	}

	public void clickOnTermsConditionsCheckBox1() {
		((JavascriptExecutor) driver).executeAsyncScript("setTimeout(arguments[arguments.length - 1], 5000);");
		termsConditionsText.click();
		termsConditionCheckBox1.click();
	}

	public void clickOnTermsConditionsCheckBox2() {
		((JavascriptExecutor) driver).executeAsyncScript("setTimeout(arguments[arguments.length - 1],4000);");
		termsConditionCheckBox2.click();
	}

	public PolicyIssuanceAndPaymentPage clickOnNextPolicyIssuanceAndPaymentBtn() {
		NextPolicyIssuanceAndPaymentBtn.click();
		return new PolicyIssuanceAndPaymentPage();
	}

	public ProductsInformationPage clickBackToFormBtn() {
		((JavascriptExecutor) driver).executeAsyncScript("setTimeout(arguments[arguments.length - 1], 8000);");
		backToFormBtn.click();
		return new ProductsInformationPage();
	}

	public boolean TermsConditionsCheckBox1IsChecked() {
		return termsConditionCheckBox1IsSelected.isSelected();
	}

	public boolean TermsConditionsCheckBox2IsChecked() {
		return termsConditionCheckBox2IsSelected.isSelected();
	}

	public boolean PolicyIssuanceAndPaymentBtnStatus() {
		return PolicyIssuanceAndPaymentBtnIsDisabled.isEnabled();
	}

	public boolean PolicyIssuanceAndPaymentBtnIsDisabledEvenWithOneCheckBox() {
		return PolicyIssuanceAndPaymentBtnIsDisabled.isEnabled();
	}

}
