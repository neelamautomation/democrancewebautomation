package com.democrance.webautomation.pageObjects;

import java.time.Duration;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.democrance.webautomation.base.BaseClass;
import com.democrance.webautomation.helper.DateUtil;
import com.democrance.webautomation.helper.LocatorUtil;

public class ProductsInformationPage extends BaseClass {

	public ProductsInformationPage() {
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, Duration.ofSeconds(10));
	}

	@FindBy(xpath = "//input[@data-e2e='field-insured_age']")
	private WebElement dateOfBirthField;

	@FindBy(xpath = "(//div[@class='control']//span/select)[1]")
	private WebElement monthDropDown;

	@FindBy(xpath = "(//div[@class='control']//span/select)[2]")
	private WebElement yearDropDown;

	@FindBy(xpath = "//input[@data-e2e='field-policy_effective_date']")
	private WebElement policyEffectiveDateField;

	@FindBy(xpath = "(//div[@class='button touch-buttons is-small is-active'])[1]")
	private WebElement salaryRangeFieldIsSelected;

	@FindBy(xpath = "(//div[@class='button touch-buttons is-small is-active'])[2]")
	private WebElement premiumPaymentOptionIsSelected;

	@FindBy(xpath = "//button[text()='Next: Your Quote']")
	private WebElement NextBtn;

	@FindBy(xpath = "//h1[text()='Information']")
	private WebElement informationText;

	public void selectDate(String month, String day, String year) {
		WebElement monthOption = LocatorUtil.createDynamicXPath(driver,
				"//div[@class='control']//select/option[normalize-space()='%s']", month);
		monthDropDown.click();
		monthOption.click();
		WebElement yearOption = LocatorUtil.createDynamicXPath(driver,
				"//div[@class='control']//select/option[normalize-space()='%s']", year);
		yearDropDown.click();
		yearOption.click();
		WebElement dayOption = LocatorUtil.createDynamicXPath(driver,
				"//a[@class='datepicker-cell is-selectable']/span[text()='%s']", day);
		dayOption.click();
	}

	public String getDateOfBirth() {
		wait.until(ExpectedConditions.visibilityOf(dateOfBirthField));
		String actualDate = dateOfBirthField.getAttribute("value");
		return actualDate;
	}

	public String getDateOfBirthInYYYYMMDD() {
		String dobFormatted = getDateOfBirth();
		return DateUtil.convertDateFromDDMMYYYYToYYYYMMDD(dobFormatted);
	}

	public void clickOnDateOfBirthCalendar() {
		dateOfBirthField.click();
	}

	public String dateOfBirthIsEmpty() {
		wait.until(ExpectedConditions.visibilityOf(dateOfBirthField));
		String DateOfBirth = dateOfBirthField.getAttribute("value");
		log.info("Value of prefilled date is " + DateOfBirth);
		return DateOfBirth;
	}

	public String getPolicyEffectiveDate() {
		String actualDate = policyEffectiveDateField.getAttribute("value");
		return actualDate;

	}

	public String getPolicyEffectiveDateInYYYYMMDD() {
		String policyEffectiveDateFormatted = getPolicyEffectiveDate();
		return DateUtil.convertDateFromDDMMYYYYToYYYYMMDD(policyEffectiveDateFormatted);
	}

	public void clickSalaryRangeField(String salaryValue) {
		WebElement salaryRangeField = LocatorUtil.createDynamicXPath(driver, "//div/span[text()='%s']", salaryValue);
		salaryRangeField.click();

	}

	public boolean SalaryRangefieldSelected() {
		return salaryRangeFieldIsSelected.isEnabled();
	}

	public String getSalaryRange() {
		return salaryRangeFieldIsSelected.getText();
	}

	public WebElement clickPremiumPaymentOption(String premiumPaymentValue) {
		WebElement premiumPaymentOption = LocatorUtil.createDynamicXPath(driver, "//div/span[text()='%s']",
				premiumPaymentValue);
		premiumPaymentOption.click();
		return premiumPaymentOption;
	}

	public String getMonthlyPremiumPaymentOption() {
		WebElement webElement = clickPremiumPaymentOption("Monthly");
		return webElement.getText();
	}

	public boolean premiumPaymentOptionSelected() {
		return premiumPaymentOptionIsSelected.isEnabled();
	}

	public TermsConditionsPage clickOnNextBtn() {
		NextBtn.click();
		return new TermsConditionsPage();
	}

	public String InformationTextVerification() {
		String actualText = informationText.getText();
		return actualText;
	}
}