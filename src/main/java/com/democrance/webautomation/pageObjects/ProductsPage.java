package com.democrance.webautomation.pageObjects;

import java.time.Duration;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.democrance.webautomation.base.BaseClass;

public class ProductsPage extends BaseClass {

	public ProductsPage() {
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, Duration.ofSeconds(3));
	}

	@FindBy(xpath = "//h1[text()='Congratulations!']")
	private WebElement productsTextVerification;

	@FindBy(xpath = "//button[@class='button coiButton']")
	private WebElement downloadPolicyBtn;

	public String productTextVerification() {
		return productsTextVerification.getText();

	}

	public String downloadPolicy() throws InterruptedException {
		downloadPolicyBtn.click();
		wait.until(ExpectedConditions.alertIsPresent());
		Alert alert = driver.switchTo().alert();
		String alertText = alert.getText();
		log.info("Alert text: " + alertText);
		alert.accept();
		return alertText;
	}

}
