package com.democrance.webautomation.pageObjects;

import java.time.Duration;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.democrance.webautomation.base.BaseClass;

public class PaymentReferencePage extends BaseClass {

	public PaymentReferencePage() {
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, Duration.ofSeconds(10));
	}

	@FindBy(xpath = "//input[@placeholder='Payment Reference']")
	private WebElement paymentReferenceTxtBox;

	@FindBy(xpath = "//span[text()='Continue']")
	private WebElement continueBtn;

	@FindBy(xpath = "//h2[text()='Enter Payment Reference']")
	private WebElement paymentReferenceTextVerification;

	public String enterDigit() {
		paymentReferenceTxtBox.sendKeys("9090909");
		String expectedPaymentReference = paymentReferenceTxtBox.getAttribute("value");
		return expectedPaymentReference;
	}

	public String paymentReferenceTextVerification() {
		wait.until(ExpectedConditions.visibilityOf(paymentReferenceTextVerification));
		return paymentReferenceTextVerification.getText();
	}

	public ProductsPage clickOnContinueBtn() {
		wait.until(ExpectedConditions.visibilityOf(continueBtn));
		continueBtn.click();
		return new ProductsPage();
	}

}
