package com.democrance.webautomation.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

	public static String getCurrentDate() {
		LocalDate currentDate = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String formattedDate = currentDate.format(formatter);
		return formattedDate;
	}

	public static String convertDateFromDDMMYYYYToYYYYMMDD(String expectedDateOfBirth) {
		String outputDateString = null;
		SimpleDateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date date = inputFormat.parse(expectedDateOfBirth);
			SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
			outputDateString = outputFormat.format(date);
			System.out.println("Output Date: " + outputDateString);
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}

		return outputDateString;
	}

	public static String getMonthNumber(String month) throws ParseException {
		Date date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(month);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int actualMonthNumber = cal.get(Calendar.MONTH) + 1;
		return String.format("%02d", actualMonthNumber);
	}

}
