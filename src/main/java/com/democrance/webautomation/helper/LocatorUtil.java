package com.democrance.webautomation.helper;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LocatorUtil {

	public static WebElement createDynamicXPath(WebDriver driver, String xpath, String dynamicValue) {
		return driver.findElement(By.xpath(String.format(xpath, dynamicValue)));
	}

}
