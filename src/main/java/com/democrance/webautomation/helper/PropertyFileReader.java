package com.democrance.webautomation.helper;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyFileReader {
	
	Properties prop;
	
	public PropertyFileReader() throws IOException {
		prop = new Properties();
		FileInputStream fis= new FileInputStream("C:\\Workspace\\Project\\DemoCranceWebAutomation\\DemocranceAutomation\\src\\test\\java\\com\\democrance\\webautomation\\data\\data.properties");               
		prop.load(fis);
	}
	
	public String readDay() {
		return prop.getProperty("day");
	}

	public String readMonth() {
		return prop.getProperty("month");
	}
	
	public String readYear() {
		return prop.getProperty("year");
	}
	
	public String readSalaryRange() {
		return prop.getProperty("salaryRange");
	}
	
	public String readPremium() {
		return prop.getProperty("premium");
	}
	
	public String readFullName() {
		return prop.getProperty("fullName");
	}
	
	public String readEmail() {
		return prop.getProperty("email");
	}
	
	public String readPhoneNumber() {
		return prop.getProperty("phoneNumber");
	}
	
	public String readEmiratesId() {
		return prop.getProperty("emiratesId");
	}
	
	public String readEmiratesExpiryDay() {
		return prop.getProperty("emiratesExpiryDay");
	}

	public String readEmiratesExpiryMonth() {
		return prop.getProperty("emiratesExpiryMonth");
	}
	
	public String readEmiratesExpiryYear() {
		return prop.getProperty("emiratesExpiryYear");
	}
	
	
}
