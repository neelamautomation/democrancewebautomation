package com.democrance.webautomation.ExtentReportListener;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class ExtentReporterNG {

	public static ExtentReports getReportobject() {
		String path = System.getProperty("user.dir") + "//reports//index.html";
		ExtentSparkReporter reporter = new ExtentSparkReporter(path);
		reporter.config().setReportName("Democrance WebAutomation Results");
		reporter.config().setDocumentTitle("Test Results");
		reporter.config().setTimeStampFormat("yyyy-MM-dd HH:mm:ss");

		ExtentReports extent = new ExtentReports();
		extent.attachReporter(reporter);
		extent.createTest(path);
		return extent;
	}
}
