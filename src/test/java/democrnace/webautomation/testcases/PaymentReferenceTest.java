package democrnace.webautomation.testcases;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.democrance.webautomation.base.BaseClass;
import com.democrance.webautomation.helper.PropertyFileReader;
import com.democrance.webautomation.pageObjects.PaymentPage;
import com.democrance.webautomation.pageObjects.PaymentReferencePage;
import com.democrance.webautomation.pageObjects.PolicyIssuanceAndPaymentPage;
import com.democrance.webautomation.pageObjects.ProductsInformationPage;
import com.democrance.webautomation.pageObjects.SummaryPage;
import com.democrance.webautomation.pageObjects.TermsConditionsPage;

public class PaymentReferenceTest extends BaseClass {

	ProductsInformationPage productsInformationPage;
	TermsConditionsPage termsConditionsPage;
	PolicyIssuanceAndPaymentPage policyIssuanceAndPaymentPage;
	SummaryPage summaryPage;
	PaymentPage paymentPage;
	PaymentReferencePage paymentReferencePage;
	String expectedDateOfBirth, expectedPolicyEffectiveDate, expectedSalaryRange, expectedPremiumPaymentOption;
	String expectedTitle, expectedFullName, expectedNationality, expectedEmailAddress, expectedMobileNumber,
			expectedEmirateID, expectedEmiratesIDExpiryDate;

	public PaymentReferenceTest() {
		super();
	}

	@BeforeMethod
	@Parameters("browser")
	public void setUp(String browser) throws IOException {
		initialization(browser);
		PropertyFileReader reader = new PropertyFileReader();
		productsInformationPage = new ProductsInformationPage();
		productsInformationPage.clickOnDateOfBirthCalendar();
		productsInformationPage.selectDate(reader.readMonth(), reader.readDay(), reader.readYear());
		productsInformationPage.getDateOfBirthInYYYYMMDD();
		productsInformationPage.getPolicyEffectiveDateInYYYYMMDD();
		productsInformationPage.clickSalaryRangeField(reader.readSalaryRange());
		productsInformationPage.getSalaryRange();
		productsInformationPage.clickPremiumPaymentOption(reader.readPremium());
		productsInformationPage.getMonthlyPremiumPaymentOption();
		productsInformationPage.premiumPaymentOptionSelected();
		termsConditionsPage = productsInformationPage.clickOnNextBtn();
		termsConditionsPage.clickOnTermsConditionsCheckBox1();
		termsConditionsPage.clickOnTermsConditionsCheckBox2();
		policyIssuanceAndPaymentPage = termsConditionsPage.clickOnNextPolicyIssuanceAndPaymentBtn();
		expectedTitle = policyIssuanceAndPaymentPage.enterTitle();
		expectedFullName = policyIssuanceAndPaymentPage.enterFullName();
		expectedNationality = policyIssuanceAndPaymentPage.enterNationality();
		expectedEmailAddress = policyIssuanceAndPaymentPage.enterEmailAddress();
		expectedMobileNumber = policyIssuanceAndPaymentPage.appendCountryCodetoMobileNumber();
		expectedEmirateID = policyIssuanceAndPaymentPage.enterEmirateID();
		policyIssuanceAndPaymentPage.selectDate(reader.readEmiratesExpiryMonth(), reader.readEmiratesExpiryDay(),
				reader.readEmiratesExpiryYear());
		expectedEmiratesIDExpiryDate = policyIssuanceAndPaymentPage.getEmiratesIdExpiryDateInYYYYMMDD();
		summaryPage = policyIssuanceAndPaymentPage.clickOnNextSummaryBtn();
		paymentPage = summaryPage.clickOnConfirmAndBuyBtn();
		paymentReferencePage = paymentPage.clickOnGenerateInvoiceOption();

	}

	@Test(priority = 1)
	public void verifyPaymentReferenceText() {
		SoftAssert softAssert = new SoftAssert();
		String actualText = paymentReferencePage.paymentReferenceTextVerification();
		softAssert.assertEquals(actualText, "Enter Payment Reference");
		softAssert.assertAll();
		log.info("The actual text is " + actualText);
	}

	@Test(priority = 2)
	public void verifyPaymentReference() {
		SoftAssert softAssert = new SoftAssert();
		String actualText = paymentReferencePage.enterDigit();
		softAssert.assertEquals(actualText, "9090909");
		softAssert.assertAll();
		log.info("The actual text is " + actualText);
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
