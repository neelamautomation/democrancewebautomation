package democrnace.webautomation.testcases;

import java.io.IOException;
import java.text.ParseException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.democrance.webautomation.base.BaseClass;
import com.democrance.webautomation.helper.DateUtil;
import com.democrance.webautomation.helper.PropertyFileReader;
import com.democrance.webautomation.pageObjects.ProductsInformationPage;

public class ProductsInformationTest extends BaseClass {

	ProductsInformationPage productsInformationPage;
	PropertyFileReader propertyFileReader;

	public ProductsInformationTest() {
		super();
	}

	@BeforeMethod
	@Parameters("browser")
	public void setUp(String browser) throws IOException {
		initialization(browser);
		productsInformationPage = new ProductsInformationPage();

	}

	@Test(priority = 1)
	public void verifyDateOfBirthFieldIsEmpty() {
		SoftAssert softAssert = new SoftAssert();
		String dateOfBirth = productsInformationPage.dateOfBirthIsEmpty();
		softAssert.assertTrue(dateOfBirth.isEmpty());
		softAssert.assertAll();
		log.info("The Date of Birth Field is empty");
	}

	@Test(priority = 2)
	public void VerifyDateOfBirthIsPrefilledWithExpectedDate() throws IOException, ParseException {
		SoftAssert softAssert = new SoftAssert();
		propertyFileReader = new PropertyFileReader();
		String day = propertyFileReader.readDay();
		String month = propertyFileReader.readMonth();
		String year = propertyFileReader.readYear();
		String expectedDateOfBirth = day + "/" + DateUtil.getMonthNumber(month) + "/" + year;
		productsInformationPage.clickOnDateOfBirthCalendar();
		productsInformationPage.selectDate(month, day, year);
		String actualDateOfBirth = productsInformationPage.getDateOfBirth();
		softAssert.assertEquals(actualDateOfBirth, expectedDateOfBirth);
		softAssert.assertAll();
		log.info("The Actual Date of Birth is" + actualDateOfBirth);
	}

	@Test(priority = 3)
	public void VerifyEffectiveDateOfPolicyIsFilledWithCurrentDate() {
		SoftAssert softAssert = new SoftAssert();
		String actualEffectiveDate = productsInformationPage.getPolicyEffectiveDate();
		String expectedEffectiveDate = DateUtil.getCurrentDate();
		softAssert.assertEquals(actualEffectiveDate, expectedEffectiveDate);
	}

	@Test(priority = 4)
	public void VerifySalaryRangeFiledIsSelected() throws IOException {
		SoftAssert softAssert = new SoftAssert();
		propertyFileReader = new PropertyFileReader();
		String salaryRange = propertyFileReader.readSalaryRange();
		productsInformationPage.clickSalaryRangeField(salaryRange);
		Boolean isSelected = productsInformationPage.SalaryRangefieldSelected();
		softAssert.assertTrue(isSelected);
		softAssert.assertAll();
	}

	@Test(priority = 5)
	public void VerifyPremiumPaymentOptionIsSelected() throws IOException {
		SoftAssert softAssert = new SoftAssert();
		propertyFileReader = new PropertyFileReader();
		String premium = propertyFileReader.readPremium();
		productsInformationPage.clickPremiumPaymentOption(premium);
		Boolean isSelected = productsInformationPage.premiumPaymentOptionSelected();
		softAssert.assertTrue(isSelected);
		softAssert.assertAll();
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
