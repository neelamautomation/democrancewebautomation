package democrnace.webautomation.testcases;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.democrance.webautomation.base.BaseClass;
import com.democrance.webautomation.helper.PropertyFileReader;
import com.democrance.webautomation.pageObjects.PolicyIssuanceAndPaymentPage;
import com.democrance.webautomation.pageObjects.ProductsInformationPage;
import com.democrance.webautomation.pageObjects.TermsConditionsPage;

public class PolicyIssuanceAndPaymentTest extends BaseClass {

	ProductsInformationPage productsInformationPage;
	TermsConditionsPage termsConditionsPage;
	PolicyIssuanceAndPaymentPage policyIssuanceAndPaymentPage;

	public PolicyIssuanceAndPaymentTest() {
		super();
	}

	@BeforeMethod
	@Parameters("browser")
	public void setUp(String browser) throws IOException {
		initialization(browser);
		PropertyFileReader reader = new PropertyFileReader();
		productsInformationPage = new ProductsInformationPage();
		productsInformationPage.clickOnDateOfBirthCalendar();
		productsInformationPage.selectDate(reader.readMonth(), reader.readDay(), reader.readYear());
		productsInformationPage.clickSalaryRangeField(reader.readSalaryRange());
		productsInformationPage.SalaryRangefieldSelected();
		productsInformationPage.clickPremiumPaymentOption(reader.readPremium());
		productsInformationPage.premiumPaymentOptionSelected();
		termsConditionsPage = productsInformationPage.clickOnNextBtn();
		termsConditionsPage.clickOnTermsConditionsCheckBox1();
		termsConditionsPage.clickOnTermsConditionsCheckBox2();
		policyIssuanceAndPaymentPage = termsConditionsPage.clickOnNextPolicyIssuanceAndPaymentBtn();
	}

	@Test(priority = 1)
	public void verifyPolicyIssuanceAndPaymentTextVerification() {
		SoftAssert softAssert = new SoftAssert();
		String actualtext = policyIssuanceAndPaymentPage.policyIssuanceAndPaymentTextVerification();
		softAssert.assertEquals(actualtext, "Policy Issuance And Payment");
		softAssert.assertAll();
		log.info("The actual text is " + actualtext);
	}

	@Test(priority = 2)
	public void verifyTitle() {
		SoftAssert softAssert = new SoftAssert();
		policyIssuanceAndPaymentPage.enterTitle();
		boolean isTitleSelected = policyIssuanceAndPaymentPage.isTitleSelected();
		softAssert.assertTrue(isTitleSelected);
		softAssert.assertAll();
	}

	@Test(priority = 3)
	public void verifyFullName() throws IOException {
		SoftAssert softAssert = new SoftAssert();
		String actualName = policyIssuanceAndPaymentPage.enterFullName();
		softAssert.assertEquals(actualName, "Neelam");
		softAssert.assertAll();
		log.info("The Entered name is" + actualName);
	}

	@Test(priority = 4)
	public void verifyNationality() {
		SoftAssert softAssert = new SoftAssert();
		String actualNationality = policyIssuanceAndPaymentPage.enterNationality();
		softAssert.assertEquals(actualNationality, "India");
		softAssert.assertAll();
		log.info("The entered Nationality is : " + actualNationality);
	}

	@Test(priority = 5)
	public void verifyEmailAddress() throws IOException {
		SoftAssert softAssert = new SoftAssert();
		String actualEmailAddress = policyIssuanceAndPaymentPage.enterEmailAddress();
		softAssert.assertEquals(actualEmailAddress, "lrajput26@gmail.com");
		softAssert.assertAll();
		log.info("The entered email address is : " + actualEmailAddress);
	}

	@Test(priority = 6)
	public void verifyMobileNumber() throws IOException {
		SoftAssert softAssert = new SoftAssert();
		String actualMobileNumber = policyIssuanceAndPaymentPage.enterMobileNumber();
		softAssert.assertEquals(actualMobileNumber, "0562267513");
		softAssert.assertAll();
		log.info("The entered Mobile Number is : " + actualMobileNumber);
	}

	@Test(priority = 7)
	public void verifyEmiratesId() throws IOException {
		SoftAssert softAssert = new SoftAssert();
		String actualEmiratesId = policyIssuanceAndPaymentPage.enterEmirateID();
		softAssert.assertEquals(actualEmiratesId, "784-1000-0000000-0");
		softAssert.assertAll();
		log.info("The entered Emirates Id is : " + actualEmiratesId);
	}

	@Test(priority = 8)
	public void verifyEmiratesExpiryDateField() {
		SoftAssert softAssert = new SoftAssert();
		policyIssuanceAndPaymentPage.selectDate("June", "27", "2027");
		String emiratesIdExpiry = policyIssuanceAndPaymentPage.getEmiratesIDExpiryDate();
		softAssert.assertEquals(emiratesIdExpiry, "27/06/2027");
		softAssert.assertAll();
		log.info("The entered Emirated id Expiry date is : " + emiratesIdExpiry);
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
