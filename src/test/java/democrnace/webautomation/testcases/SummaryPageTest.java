package democrnace.webautomation.testcases;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.democrance.webautomation.base.BaseClass;
import com.democrance.webautomation.helper.PropertyFileReader;
import com.democrance.webautomation.pageObjects.PolicyIssuanceAndPaymentPage;
import com.democrance.webautomation.pageObjects.ProductsInformationPage;
import com.democrance.webautomation.pageObjects.SummaryPage;
import com.democrance.webautomation.pageObjects.TermsConditionsPage;

public class SummaryPageTest extends BaseClass{
	
	ProductsInformationPage productsInformationPage;
	TermsConditionsPage termsConditionsPage;
	PolicyIssuanceAndPaymentPage policyIssuanceAndPaymentPage;
	SummaryPage summaryPage;
	String expectedDateOfBirth,expectedPolicyEffectiveDate,expectedSalaryRange,expectedPremiumPaymentOption;
	String expectedTitle,expectedFullName,expectedNationality,expectedEmailAddress,expectedMobileNumber,expectedEmirateID,expectedEmiratesIDExpiryDate;
	
	
	public SummaryPageTest() {
		super();
	}

	@BeforeMethod
	@Parameters("browser")
	public void setUp(String browser) throws IOException {
		initialization(browser);
		PropertyFileReader reader = new PropertyFileReader();
		productsInformationPage= new ProductsInformationPage();
		productsInformationPage.clickOnDateOfBirthCalendar();
		productsInformationPage.selectDate(reader.readMonth(),reader.readDay(), reader.readYear());
		expectedDateOfBirth=productsInformationPage.getDateOfBirthInYYYYMMDD();
		expectedPolicyEffectiveDate=productsInformationPage.getPolicyEffectiveDateInYYYYMMDD();
		productsInformationPage.clickSalaryRangeField(reader.readSalaryRange());
		expectedSalaryRange=productsInformationPage.getSalaryRange();
		productsInformationPage.clickPremiumPaymentOption(reader.readPremium());
		expectedPremiumPaymentOption=productsInformationPage.getMonthlyPremiumPaymentOption();
		productsInformationPage.premiumPaymentOptionSelected();
		termsConditionsPage=productsInformationPage.clickOnNextBtn();
		termsConditionsPage.clickOnTermsConditionsCheckBox1();
		termsConditionsPage.clickOnTermsConditionsCheckBox2();
		policyIssuanceAndPaymentPage=termsConditionsPage.clickOnNextPolicyIssuanceAndPaymentBtn();
		//Insurer details
		expectedTitle=policyIssuanceAndPaymentPage.enterTitle();
		expectedFullName=policyIssuanceAndPaymentPage.enterFullName();
		expectedNationality=policyIssuanceAndPaymentPage.enterNationality();
		expectedEmailAddress=policyIssuanceAndPaymentPage.enterEmailAddress();
		expectedMobileNumber=policyIssuanceAndPaymentPage.appendCountryCodetoMobileNumber();
		expectedEmirateID=policyIssuanceAndPaymentPage.enterEmirateID();
		policyIssuanceAndPaymentPage.selectDate(reader.readEmiratesExpiryMonth(), reader.readEmiratesExpiryDay(),reader.readEmiratesExpiryYear());
		expectedEmiratesIDExpiryDate=policyIssuanceAndPaymentPage.getEmiratesIdExpiryDateInYYYYMMDD();
		summaryPage=policyIssuanceAndPaymentPage.clickOnNextSummaryBtn();
	}
		
	@Test(priority=1)
	public void verifyDateOfBirthOnSummaryPage() {	
		String actualDateOfBirth=summaryPage.getDateOfBirth();
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(actualDateOfBirth,expectedDateOfBirth);
		softAssert.assertAll();
		log.info("The actual date of birth under Information tab is"+actualDateOfBirth);
	}
	
	@Test(priority=2)
	public void verifyPolicyEffectiveDateOnSummaryPage() {	
		String actualPolicyEffectiveDate=summaryPage.getPolicyEffectiveDate();
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(actualPolicyEffectiveDate,expectedPolicyEffectiveDate);
		softAssert.assertAll();
		log.info("The actual policy effective date under Information tab is"+actualPolicyEffectiveDate);
	}
	
	@Test(priority=3)
	public void verifySalaryRangeOnSummaryPage() {	
		String actualSalaryRange=summaryPage.getSalaryRange();
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(actualSalaryRange,expectedSalaryRange);
		softAssert.assertAll();
		log.info("The actual salary range under Information tab is"+actualSalaryRange);
	}
	
	@Test(priority=4)
	public void verifyPremiumPaymentOptionOnSummaryPage() {	
		String actualPremiumPaymentOption=summaryPage.getPremiumPaymentOption();
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(actualPremiumPaymentOption,expectedPremiumPaymentOption);
		softAssert.assertAll();
		log.info("The actual premium payment option under Information tab is"+actualPremiumPaymentOption);
	}
	
	@Test(priority=5)
	public void verifyTitleOnSummaryPage() {
		SoftAssert softAssert = new SoftAssert();
		String actualTitle=summaryPage.getTitle();
		softAssert.assertEquals(actualTitle,expectedTitle);
		softAssert.assertAll();
	}
		
	@Test(priority=6)
	public void verifyFullNameOnSummaryPage() {
		SoftAssert softAssert = new SoftAssert();
		String actualFullName=summaryPage.getFullName();
		softAssert.assertEquals(actualFullName,expectedFullName);
		softAssert.assertAll();
	}
	
	@Test(priority=7)
	public void verifyNationalityOnSummaryPage() {
		SoftAssert softAssert = new SoftAssert();
		String actualNationality=summaryPage.getNationality();
		softAssert.assertEquals(actualNationality,expectedNationality);
		softAssert.assertAll();
	}
	
	@Test(priority=8)
	public void verifyEmailAddressOnSummaryPage() {
		SoftAssert softAssert = new SoftAssert();
		String actualEmailAddress=summaryPage.getEmailAddress();
		softAssert.assertEquals(actualEmailAddress,expectedEmailAddress);
		softAssert.assertAll();
	}
	
	@Test(priority=9)
	public void verifyMobileNumberOnSummaryPage() {
		SoftAssert softAssert = new SoftAssert();
		String actualMobileNumber=summaryPage.getMobileNumber();
		softAssert.assertEquals(actualMobileNumber,expectedMobileNumber);
		softAssert.assertAll();
	}
	
	@Test(priority=10)
	public void verifyEmiratesIdNumberOnSummaryPage() {
		SoftAssert softAssert = new SoftAssert();
		String actualEmiratesIdNumber=summaryPage.getEmiratesIdNumber();
		softAssert.assertEquals(actualEmiratesIdNumber,expectedEmirateID);
		softAssert.assertAll();
	}
	
	@Test(priority=11)
	public void verifyEmiratesIdExpiryDateOnSummaryPage() {
		SoftAssert softAssert = new SoftAssert();
		String actualEmiratesIdExpiryDate=summaryPage.getEmiratesIdExpiryDate();
		softAssert.assertEquals(actualEmiratesIdExpiryDate,expectedEmiratesIDExpiryDate);
		softAssert.assertAll();
	}
	
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
	
	
	 
}
