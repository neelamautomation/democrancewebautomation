package democrnace.webautomation.testcases;



import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.democrance.webautomation.base.BaseClass;
import com.democrance.webautomation.helper.PropertyFileReader;
import com.democrance.webautomation.pageObjects.PaymentPage;
import com.democrance.webautomation.pageObjects.PaymentReferencePage;
import com.democrance.webautomation.pageObjects.PolicyIssuanceAndPaymentPage;
import com.democrance.webautomation.pageObjects.ProductsInformationPage;
import com.democrance.webautomation.pageObjects.ProductsPage;
import com.democrance.webautomation.pageObjects.SummaryPage;
import com.democrance.webautomation.pageObjects.TermsConditionsPage;

public class ProductTest extends BaseClass{
	
	ProductsInformationPage productsInformationPage;
	TermsConditionsPage termsConditionsPage;
	PolicyIssuanceAndPaymentPage policyIssuanceAndPaymentPage;
	SummaryPage summaryPage;
	PaymentPage paymentPage;
	PaymentReferencePage paymentReferencePage;
	ProductsPage productsPage;
	String expectedDateOfBirth,expectedPolicyEffectiveDate,expectedSalaryRange,expectedPremiumPaymentOption;
	String expectedTitle,expectedFullName,expectedNationality,expectedEmailAddress,expectedMobileNumber,expectedEmirateID,expectedEmiratesIDExpiryDate;
	
	
	public ProductTest() {
		super();
	}

	@BeforeMethod
	@Parameters("browser")
	public void setUp(String browser) throws IOException {
		initialization(browser);
		PropertyFileReader reader = new PropertyFileReader();
		productsInformationPage= new ProductsInformationPage();
		productsInformationPage.clickOnDateOfBirthCalendar();
		productsInformationPage.selectDate(reader.readMonth(),reader.readDay(), reader.readYear());
		productsInformationPage.getDateOfBirthInYYYYMMDD();
		productsInformationPage.getPolicyEffectiveDateInYYYYMMDD();
		productsInformationPage.clickSalaryRangeField(reader.readSalaryRange());
        productsInformationPage.getSalaryRange();
		productsInformationPage.clickPremiumPaymentOption(reader.readPremium());
		productsInformationPage.getMonthlyPremiumPaymentOption();
		productsInformationPage.premiumPaymentOptionSelected();
		termsConditionsPage=productsInformationPage.clickOnNextBtn();
		termsConditionsPage.clickOnTermsConditionsCheckBox1();
		termsConditionsPage.clickOnTermsConditionsCheckBox2();
		policyIssuanceAndPaymentPage=termsConditionsPage.clickOnNextPolicyIssuanceAndPaymentBtn();
		expectedTitle=policyIssuanceAndPaymentPage.enterTitle();
		expectedFullName=policyIssuanceAndPaymentPage.enterFullName();
		expectedNationality=policyIssuanceAndPaymentPage.enterNationality();
		expectedEmailAddress=policyIssuanceAndPaymentPage.enterEmailAddress();
		expectedMobileNumber=policyIssuanceAndPaymentPage.appendCountryCodetoMobileNumber();
		expectedEmirateID=policyIssuanceAndPaymentPage.enterEmirateID();
		policyIssuanceAndPaymentPage.selectDate(reader.readEmiratesExpiryMonth(), reader.readEmiratesExpiryDay(),reader.readEmiratesExpiryYear());
		expectedEmiratesIDExpiryDate=policyIssuanceAndPaymentPage.getEmiratesIdExpiryDateInYYYYMMDD();
		summaryPage=policyIssuanceAndPaymentPage.clickOnNextSummaryBtn();
		paymentPage=summaryPage.clickOnConfirmAndBuyBtn();
		paymentReferencePage=paymentPage.clickOnGenerateInvoiceOption();
		paymentReferencePage.enterDigit();
		productsPage=paymentReferencePage.clickOnContinueBtn();
	}
		
   @Test(priority=1)
	public void verifyPaymentOptionText() throws InterruptedException {	
		productsPage.downloadPolicy();
		String actualText=productsPage.productTextVerification();
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(actualText,"Congratulations!");
		softAssert.assertAll();
		log.info("The actual text is"+actualText);
	}
	
	@Test(priority=2)
	public void VerifyDownLoadPolicy() throws InterruptedException {	
		String actualAlertText=productsPage.downloadPolicy();
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertTrue(actualAlertText.equalsIgnoreCase("Request failed with status code 403"), 
                "Download failed: Request failed with status code 403");
		softAssert.assertAll();
		log.info("The actual text is"+actualAlertText);
	}
	

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}

