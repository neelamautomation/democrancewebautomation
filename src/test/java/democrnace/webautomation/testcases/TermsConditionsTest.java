package democrnace.webautomation.testcases;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.democrance.webautomation.base.BaseClass;
import com.democrance.webautomation.helper.PropertyFileReader;
import com.democrance.webautomation.pageObjects.ProductsInformationPage;
import com.democrance.webautomation.pageObjects.TermsConditionsPage;

public class TermsConditionsTest extends BaseClass {

	ProductsInformationPage productsInformationPage;
	TermsConditionsPage termsConditionsPage;

	public TermsConditionsTest() {
		super();
	}

	@BeforeMethod
	@Parameters("browser")
	public void setUp(String browser) throws IOException {
		initialization(browser);
		PropertyFileReader reader = new PropertyFileReader();
		productsInformationPage = new ProductsInformationPage();
		productsInformationPage.clickOnDateOfBirthCalendar();
		productsInformationPage.selectDate(reader.readMonth(), reader.readDay(), reader.readYear());
		productsInformationPage.clickSalaryRangeField(reader.readSalaryRange());
		productsInformationPage.SalaryRangefieldSelected();
		productsInformationPage.clickPremiumPaymentOption(reader.readPremium());
		productsInformationPage.premiumPaymentOptionSelected();
		termsConditionsPage = productsInformationPage.clickOnNextBtn();
	}

	@Test(priority = 1)
	public void verifyTermsAndConditionsTextVerification() {
		SoftAssert softAssert = new SoftAssert();
		String actualtext = termsConditionsPage.TermsAndConditionsTextVerification();
		softAssert.assertEquals(actualtext, "Terms & Conditions");
		softAssert.assertAll();
	}

	@Test(priority = 2)
	public void verifyTermsAndConditionsCheckBox1IsSelected() {
		SoftAssert softAssert = new SoftAssert();
		termsConditionsPage.clickOnTermsConditionsCheckBox1();
		Boolean isChecked = termsConditionsPage.TermsConditionsCheckBox1IsChecked();
		softAssert.assertTrue(isChecked);
		softAssert.assertAll();
	}

	@Test(priority = 3)
	public void verifyTermsAndConditionsCheckBox2IsSelected() {
		SoftAssert softAssert = new SoftAssert();
		termsConditionsPage.clickOnTermsConditionsCheckBox2();
		Boolean isChecked = termsConditionsPage.TermsConditionsCheckBox2IsChecked();
		softAssert.assertTrue(isChecked);
		softAssert.assertAll();
	}

	@Test(priority = 4)
	public void verifyBackToFormBtn() {
		SoftAssert softAssert = new SoftAssert();
		productsInformationPage = termsConditionsPage.clickBackToFormBtn();
		String actualText = productsInformationPage.InformationTextVerification();
		softAssert.assertEquals(actualText, "Information");
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
