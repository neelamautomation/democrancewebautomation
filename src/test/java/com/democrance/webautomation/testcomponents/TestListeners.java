package com.democrance.webautomation.testcomponents;

import java.io.IOException;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.democrance.webautomation.ExtentReportListener.ExtentReporterNG;
import com.democrance.webautomation.base.BaseClass;

public class TestListeners extends BaseClass implements ITestListener {

	ExtentReports extent = ExtentReporterNG.getReportobject();
	ExtentTest test;

	@Override
	public void onTestStart(ITestResult result) {
		String testName = result.getName();
		System.out.println(testName + "Started");
		test = extent.createTest(testName);
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		String testName = result.getName();
		test.log(Status.PASS, "Test is passed" + testName);

	}

	@Override
	public void onTestFailure(ITestResult result) {
		String filePath = null;
		test.fail(result.getThrowable());
		String testName = result.getName();
		try {
			filePath = getScreenshot(testName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		test.addScreenCaptureFromPath(filePath, testName);

	}

	@Override
	public void onFinish(ITestContext context) {
		extent.flush();
	}

}
